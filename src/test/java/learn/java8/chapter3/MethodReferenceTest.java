package learn.java8.chapter3;

import junit.framework.TestCase;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * 方法引用
 *
 * @author tanghc
 */
public class MethodReferenceTest extends TestCase {
    static class Person {
        public static void sayHello(String name) {
            System.out.println("hello " + name);
        }
    }

    static class Goods {
        private int price;

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }
    }

    static class GoodsService {
        private Goods goods;

        public GoodsService(Goods goods) {
            super();
            this.goods = goods;
        }

        public void showPrice(Function<Goods, Integer> fun) {
            int price = fun.apply(goods);
            System.out.println("price:" + price);
        }
    }

    static class Dog {
        private int age;

        public Dog(int age) {
            super();
            this.age = age;
        }

        public void say(String name) {
            System.out.println("dog age is " + age + ", dog name is " + name);
        }
    }


    // 静态方法引用
    public void test1() {
        work(String::toString);
        work(String::toLowerCase);
    }

    // 自身类型方法引用
    public void testService() {
        Goods goodsApple = new Goods();
        goodsApple.setPrice(100);

        GoodsService service = new GoodsService(goodsApple);
        // 方式1：使用Lambda表达式
        service.showPrice((Goods goods) -> {
            return goods.getPrice();
        });

        // 方式2：使用方法引用
        service.showPrice(Goods::getPrice);
    }

    // 自身类型方法引用
    public void testSelf() {
        print("hello", (String s) -> s.toLowerCase());

        print("hello", String::toLowerCase);
    }

    // 现有实例方法引用
    public void testInstance() {
        Dog dog = new Dog(3);
        // 方式1，使用Lambda表达式
        print("Tom", (String s) -> dog.say(s));
        // 方式2，实例对象方法引用
        print("Tom", dog::say);
    }


    public static void work(Consumer<String> consumer) {
        consumer.accept("Jim");
    }

    private static void print(String argu, Consumer<String> consumer) {
        consumer.accept(argu);
    }
}
