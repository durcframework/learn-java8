package learn.java8.chapter1;

import junit.framework.TestCase;

import java.util.function.Consumer;

/**
 * @author tanghc
 */
public class PersonTest extends TestCase {
    static public class Person {
        public static void sayHello(String name) {
            System.out.println("hello " + name);
        }
    }

    // 自定义函数式接口
    @FunctionalInterface
    public interface FunctionCaller<T> {
        void call(T t);
    }

    public void testFun() {
        work(Person::sayHello);
    }

    public void testSelfFun() {
        workFun(Person::sayHello);
    }

    public static void workFun(FunctionCaller<String> caller) {
        caller.call("Jim");
    }

    public static void work(Consumer<String> consumer) {
        consumer.accept("Jim");
    }
}
