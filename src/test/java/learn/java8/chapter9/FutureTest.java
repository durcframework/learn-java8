package learn.java8.chapter9;

import junit.framework.TestCase;

import java.util.concurrent.*;

public class FutureTest extends TestCase {
    // 申明一个线程池
    ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(5,
            5,
            0,
            TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<Runnable>());

    public void testBook() {
        String bookName = "《飘》";
        System.out.println("小明去图书馆借书");
        Future<String> future = threadPoolExecutor.submit(() -> {
            // 模拟图书管理员找书花费时间
            long minutes = (long) (Math.random() * 10) + 1;
            System.out.println("图书管理员花费了" + minutes + "分钟，找到了图书" + bookName);
            Thread.sleep((long) (Math.random() * 2000));
            return bookName;
        });
        // 等待过程中做其他事情
        this.playPhone();
        try {
//            String book = future.get();
            // 等待2分钟，如果2分钟后还是没有拿到图书
            // throw TimeoutException
            String book = future.get(2, TimeUnit.MINUTES);
            System.out.println("小明拿到了图书" + book);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

    private void playPhone() {
        System.out.println("小明在玩手机等待图书");
    }

}
