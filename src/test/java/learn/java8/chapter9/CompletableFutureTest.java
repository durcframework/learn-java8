package learn.java8.chapter9;

import junit.framework.TestCase;

import java.util.concurrent.*;

public class CompletableFutureTest extends TestCase {

    public void testBook() {
        String bookName = "《飘》";
        System.out.println("小明去图书馆借书");
        CompletableFuture<String> future = new CompletableFuture<>();
        new Thread(() -> {
            // 模拟图书管理员找书花费时间
            long minutes = (long) (Math.random() * 10) + 1;
            System.out.println("图书管理员花费了" + minutes + "分钟，找到了图书" + bookName);
            try {
                Thread.sleep((long) (Math.random() * 2000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            future.complete(bookName);
        }).start();
        // 等待过程中做其他事情
        this.playPhone();
        try {
            String book = future.get();
            System.out.println("小明拿到了图书" + book);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private void playPhone() {
        System.out.println("小明在玩手机等待图书");
    }

}
