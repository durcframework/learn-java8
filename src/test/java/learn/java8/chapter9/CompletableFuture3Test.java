package learn.java8.chapter9;

import junit.framework.TestCase;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class CompletableFuture3Test extends TestCase {

    public void testThenApply() throws ExecutionException, InterruptedException {
        int i = 0;
        CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> i + 1)
                .thenApply(String::valueOf);
        String str = future.get();
        System.out.println("String value: " + str);
    }

    public void testThenCompose() throws ExecutionException, InterruptedException {
        int i=0;
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> i + 1)
                .thenCompose((j) -> CompletableFuture.supplyAsync(() -> j + 2));
        Integer result = future.get();
    }

    public void testThenCombine() throws ExecutionException, InterruptedException {
        int i=0;
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> i + 1)
                .thenCombine(CompletableFuture.supplyAsync(() -> i + 2), (result1, result2) -> {
                    System.out.println("第一个CompletableFuture结果：" + result1);
                    System.out.println("第二个CompletableFuture结果：" + result2);
                    return result1 + result2;
                });
        Integer total = future.get();
        System.out.println("总和：" + total);
    }

    public void testJoin() {
        List<CompletableFuture> futures = new ArrayList<>();
        System.out.println("100米跑步比赛开始");
        for (int i = 0; i < 10; i++) {
            final int num = i + 1;
            futures.add(CompletableFuture.runAsync(() -> {
                int v =  (int)(Math.random() * 10);
                try {
                    Thread.sleep(v);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(num + "号选手到达终点，用时：" + (10 + v) + "秒");
            }));
        }
        CompletableFuture<Double>[] futureArr = futures.toArray(new CompletableFuture[futures.size()]);
        CompletableFuture.allOf(futureArr).join();
        System.out.printf("所有选手到达终点");
    }

    public void testException() {
        CompletableFuture<String> future = new CompletableFuture<>();
        future.whenCompleteAsync((value, e)->{
            if (e != null) {
                // 捕获异常2，这里也会打印
                System.out.println("捕获异常2, msg:" + e.getMessage());
            } else {
                System.out.println("返回结果：" + value);
            }
        });
        new Thread(()->{
            String value = "http://";
            try {
                // 模拟出错，给一个不存在的ENCODE
                value = URLEncoder.encode(value, "UTF-88");
                future.complete(value);
            } catch (UnsupportedEncodingException e) {
                // future处理异常
                future.completeExceptionally(e);
                // 此方式调用者无法捕获异常
//                throw new RuntimeException(e);
            }
        }).start();
        try {
            future.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            // 捕获异常1，首先会到这里来
            System.out.println("捕获异常1，msg:" + e.getMessage());
        }
    }

    public void testException2() {
        CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> {
            String value = "http://";
            try {
                // 模拟出错，给一个不存在的ENCODE
                value = URLEncoder.encode(value, "UTF-88");
                return value;
            } catch (UnsupportedEncodingException e) {
                // 这样不行，可以throw
                //future.completeExceptionally(e);
                throw new RuntimeException(e);
            }
        });
        try {
            future.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            // 捕获异常1，首先会到这里来
            System.out.println("捕获异常1,msg:" + e.getMessage());
        }
    }

}
