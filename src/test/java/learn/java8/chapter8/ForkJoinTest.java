package learn.java8.chapter8;

import junit.framework.TestCase;
import lombok.AllArgsConstructor;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
import java.util.stream.LongStream;

public class ForkJoinTest extends TestCase {

    public void test1() {
        long startTime = System.currentTimeMillis();
        // 生成一个数组，存放1,2,3,4....
        long[] numbers = LongStream.rangeClosed(1, 1000000).toArray();
        // 创建一个任务，起始位置是0，结束位置是数组的长度
        NumberAddTask numberAddTask = new NumberAddTask(numbers, 0, numbers.length);
        // 将任务加入到线程池中运行，得到总和
        Long sum = new ForkJoinPool().invoke(numberAddTask);
        long time = System.currentTimeMillis() - startTime;
        System.out.println("sum:" + sum + ", 耗时:" + time + "毫秒");
    }

    static class A extends ForkJoinPool {

    }

    static class B extends RecursiveTask<Long> {

        @Override
        protected Long compute() {
            return null;
        }
    }

    @AllArgsConstructor
    static class NumberAddTask extends RecursiveTask<Long> {

        // 存放数字
        private long[] numbers;
        // 计算的起始位置
        private int startIndex;
        // 计算的结束位置
        private int endIndex;

        @Override
        protected Long compute() {
            int len = endIndex - startIndex;
            // 无法拆分了，开始运算
            if (len <= 10) {
                return execute();
            }
            // 拆分左边的子任务
            NumberAddTask leftTask = new NumberAddTask(numbers, startIndex, startIndex + len / 2);
            // 将子任务加入到ForkJoinPool中去
            leftTask.fork();
            // 创建右边的任务
            NumberAddTask rightTask = new NumberAddTask(numbers, startIndex + len / 2, endIndex);
            // 执行右边的任务
            Long rightSum = rightTask.compute();
            // 读取左边的子任务结果，这里会阻塞
            Long leftSum = leftTask.join();
            // 合并结果
            return leftSum + rightSum;
        }

        private long execute() {
            long sum = 0;
            for (int i = startIndex; i < endIndex; i++) {
                sum += numbers[i];
            }
            return sum;
        }
    }

}
