package learn.java8.chapter8;

import junit.framework.TestCase;

import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.stream.LongStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * @author tanghc
 */
public class SpliteratorTest extends TestCase {

    public void testDo() {
        // 创建一个数组
        long[] numbers = LongStream.rangeClosed(1, 1000000).toArray();
        long startTime = System.currentTimeMillis();
        LongSpliterator spliterator = new LongSpliterator(numbers, 0, numbers.length);
        // 申明一个并行的Stream
        Stream<Long> stream = StreamSupport.stream(spliterator, true);
        // 计算从1加到1000000，结果应该是：500000500000
        Long sum = stream.reduce((n1, n2) -> n1 + n2).orElse(0L);
        long time = System.currentTimeMillis() - startTime;
        System.out.println("sum:" + sum + ", 耗时:" + time + "毫秒");
    }

    static class LongSpliterator implements Spliterator<Long> {

        private long[] array;
        private int index;
        private int end;

        public LongSpliterator(long[] array, int index, int end) {
            this.array = array;
            this.index = index;
            this.end = end;
        }

        @Override
        public boolean tryAdvance(Consumer<? super Long> action) {
            if (index >= 0 && index < end) {
                // 取出元素
                Long l = array[index++];
                // 执行
                action.accept(l);
                return true;
            }
            return false;
        }

        /**
         * 尝试分割集合。
         * 切割规则：数组一分为2，留后半段，将前半段再一分为2。
         * @return 返回null结束分割
         */
        @Override
        public Spliterator<Long> trySplit() {
            int start = 0;
            // 取中间
            int middle = (start + end) >>> 1;
            if (start < middle) {
                return null;
            }
            // 当前index变成中间值，即当前类的操作范围是：middle ~ end
            index = middle;
            // 将前半段再一分为2
            return new LongSpliterator(array, start, middle);
        }

        @Override
        public long estimateSize() {
            return end - index;
        }

        @Override
        public int characteristics() {
            return Spliterator.SIZED | Spliterator.SUBSIZED;
        }
    }
}
