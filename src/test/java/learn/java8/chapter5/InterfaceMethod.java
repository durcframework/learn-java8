package learn.java8.chapter5;

public interface InterfaceMethod {
	default void say() {
		System.out.println("hello");
	}
	
	default int getAge() {
		return 1;
	}
}
