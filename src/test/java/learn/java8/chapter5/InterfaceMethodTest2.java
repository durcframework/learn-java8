package learn.java8.chapter5;

public class InterfaceMethodTest2 {

	interface A {
		default void say() {
			System.out.println("A");
		}
	}

	interface B extends A {
		default void say() {
			System.out.println("B");
		}
	}
	
	static class MyClass implements A, B {
		public static void main(String[] args) {
			new MyClass().say();
		}
	}
}
