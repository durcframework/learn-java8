package learn.java8.chapter5;

public class InterfaceMethodTest3 {

	interface A {
		default void say() {
			System.out.println("A");
		}
	}

	interface B {
		default void say() {
			System.out.println("B");
		}
	}

	static class MyClass implements A, B {
		@Override
		public void say() {
			A.super.say();
		}

		public static void main(String[] args) {
			new MyClass().say();
		}
	}
}
