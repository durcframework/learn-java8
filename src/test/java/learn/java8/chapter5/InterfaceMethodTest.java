package learn.java8.chapter5;

public class InterfaceMethodTest {

	interface A {
		default void say() {
			System.out.println("A");
		}
	}

	interface B {
		void say();
	}

	static class MyClass implements A, B {
		@Override
		public void say() {
			// 调用A接口的中的默认方法
			A.super.say();
			System.out.println("myclass");
		}
		
		public static void main(String[] args) {
			new MyClass().say();
		}
	}

}
