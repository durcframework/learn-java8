package learn.java8.chapter5;

interface MyInterface {
	default void say() {
		System.out.println("hello");
	}
}

public class MyClass implements MyInterface {
	@Override
	public void say() {
		System.out.println("goodbye");
		MyInterface.super.say();
	}
	
	public static void main(String[] args) {
		new MyClass().say();
	}
}
