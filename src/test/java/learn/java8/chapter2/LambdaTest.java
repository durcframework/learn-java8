package learn.java8.chapter2;

import junit.framework.TestCase;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author tanghc
 */
public class LambdaTest extends TestCase {

    public void test1() throws InterruptedException, ExecutionException {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("run...");
            }
        });
        thread.start();
        Thread.sleep(100);

        // 方式2
        Thread thread2 = new Thread(() -> System.out.println("run2..."));
        thread2.start();
        Thread.sleep(100);

        ExecutorService excutorService = Executors.newSingleThreadExecutor();
        Future<String> future = excutorService.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return "Jim";
            }
        });
        String name = future.get();
        System.out.println(name);

        excutorService = Executors.newSingleThreadExecutor();
        future = excutorService.submit(() -> {
            return "Jim2";
        });
        name = future.get();
        System.out.println(name);
    }

}
