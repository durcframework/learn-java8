package learn.java8.chapter2;

import junit.framework.TestCase;

/**
 * 自定义函数式接口
 * @author tanghc
 */
public class LambdaTest2 extends TestCase {
    @FunctionalInterface
    interface Caller {
        String call(String name);
    }

    static class Serivce {
        private String name;

        public Serivce(String name) {
            this.name = name;
        }

        public String run(Caller caller) {
            return caller.call(name);
        }
    }

    public void test1() {
        Serivce service = new Serivce("Jim");
        String result = service.run((String name) -> {
            return "My name is " + name;
        });
        System.out.println(result);
    }

}
