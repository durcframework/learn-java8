package learn.java8.chapter6;

import junit.framework.TestCase;
import lombok.Data;

import java.util.Optional;

/**
 * @author tanghc
 */
public class OptionalTest extends TestCase {

    public void test1() {
        String s = "hello";
        Optional<String> optional = Optional.of(s);
        if (optional.isPresent()) {
            System.out.println("the value is " + optional.get());
        }

        optional.ifPresent((val) -> {
            System.out.println("the value is " + val);
        });


        boolean exist = optional
                .filter(val -> "hello1".equals(val))
                .isPresent();
        System.out.println(exist);
    }


    @Data
    static class Goods {
        private String goodsName;
    }

    public void test2() {
        Goods goods = new Goods();
        goods.setGoodsName("iphoneX");
        Optional<Goods> optional = Optional.of(goods);
        Optional<String> optionalName = optional.map(Goods::getGoodsName);
    }

    public void test3() {
        String s = "1";
        Optional<String> optional = Optional.ofNullable(s);
        System.out.println(optional.orElse(getDefault()));
    }

    private String getDefault() {
        System.out.println("生成了字符串对象");
        return "1";
    }



}
