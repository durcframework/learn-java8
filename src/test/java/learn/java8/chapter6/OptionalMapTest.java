package learn.java8.chapter6;

import lombok.Data;

import java.util.Optional;

public class OptionalMapTest {
	@Data
	static class Goods {
		private String goodsName;
		private Company company;
	}

	@Data
	static class Company {
		private String companyName;
	}

	public static void main(String[] args) {
		Company company = new Company();
		company.setCompanyName("Apple");
		Goods goods = new Goods();
		goods.setGoodsName("iphoneX");
		goods.setCompany(company);
		
		Optional<Goods> optional = Optional.of(goods);
		String companyName = optional
				// 从goods中取出Company，返回一个新的Optional<Company>
				.map(goodsObj -> goodsObj.getCompany())
				// 从company中取出companyName，返回一个新的Optional<String>
				.map(companyObj -> companyObj.getCompanyName())
				// 得到companyName
				.get();
		System.out.println(companyName);
	}
	
}
