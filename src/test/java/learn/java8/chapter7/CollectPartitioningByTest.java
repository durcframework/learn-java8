package learn.java8.chapter7;

import junit.framework.TestCase;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class CollectPartitioningByTest extends TestCase {

	@Data
	@AllArgsConstructor
	static class Goods {
		private String goodsName;
		// 类型，1：手机，2：电脑
		private int type;
		@Override
		public String toString() {
			return goodsName;
		}
	}
	
	public void test1() {
		List<Goods> list = Arrays.asList(
				new Goods("iphoneX", 1)
				, new Goods("mate30 pro", 1)
				, new Goods("thinkpad T400", 2)
				, new Goods("macbook pro", 2)
				);
		
		// 手机归为一类，非手机商品归为一类
		// true -> 手机类商品
		// false -> 非手机类商品
		Map<Boolean, Set<Goods>> goodsMap = list.stream()
			.collect(
				Collectors.partitioningBy(
					goods -> goods.getType() == 1
					, Collectors.toSet())
			);
		// 获取手机类商品
		Collection<Goods> mobileGoods = goodsMap.get(true);
		System.out.println(mobileGoods);
	}

}
