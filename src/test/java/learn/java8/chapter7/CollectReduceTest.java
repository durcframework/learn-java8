package learn.java8.chapter7;

import java.util.Optional;
import java.util.stream.Stream;

import junit.framework.TestCase;

public class CollectReduceTest extends TestCase {

	public void testReduce() {
		Optional<Integer> opt = Stream.of(1, 2, 3, 4)
				.reduce((n1, n2) -> {
					int ret = n1 + n2;
					System.out.println(n1 + "(n1) + " + n2 + "(n2) = " + ret);
					return ret;
				});
		int sum = opt.orElse(0);
		System.out.println("sum:" + sum);
	}

	public void testFor() {
		int[] arr = { 1, 2, 3, 4};
		int sum = 0;
		for (int i : arr) {
			sum += i;
		}
		System.out.println("sum:" + sum);
	}

	public void testReduce2() {
		int sum = Stream.of(1, 2, 3).reduce(0, (n1, n2) -> n1 + n2);
		System.out.println("sum:" + sum);
	}

}
