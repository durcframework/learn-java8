package learn.java8.chapter7;

import junit.framework.TestCase;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author tanghc
 */
public class CollectGroupingbyTest extends TestCase {

    @Data
    @AllArgsConstructor
    static class Goods {
        private String goodsName;
        // 类型，1：手机，2：电脑
        private int type;
        @Override
        public String toString() {
            return goodsName;
        }
    }

    public void test1() {
        List<Goods> list = Arrays.asList(
				new Goods("iphoneX", 1)
				, new Goods("mate30 pro", 1)
				, new Goods("thinkpad T400", 2)
				, new Goods("macbook pro", 2)
				);

		Map<Integer, List<Goods>> goodsListMap = list.stream()
			.collect(
			        Collectors.groupingBy(Goods::getType)
            );
		goodsListMap.forEach((key, value) -> {
			System.out.println("类型" + key + ":" + value);
		});
    }

    public void test2() {
        List<Goods> list = Arrays.asList(
				new Goods("iphoneX", 1)
				, new Goods("mate30 pro", 1)
				, new Goods("thinkpad T400", 2)
				, new Goods("macbook pro", 2)
				);

		Map<Integer, List<String>> goodsListMap = list.stream()
			.collect(
			        Collectors.groupingBy(
			                Goods::getType
                            , Collectors.mapping(Goods::getGoodsName, Collectors.toList())
                    )
            );
		goodsListMap.forEach((key, value) -> {
			System.out.println("类型" + key + ":" + value);
		});
    }
}
