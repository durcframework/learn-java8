package learn.java8.chapter7;

import junit.framework.TestCase;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class StreamTest extends TestCase {

	@Data
	@AllArgsConstructor
	static class Goods {
		private String goodsName;
		private int price;
	}

	public void testCreate() {
		String[] arr = {"hello", "world"};
		Stream streamArr = Stream.of(arr);
		
		String str = "hello world";
		Stream streamSingle = Stream.of(str);
		
		Collection<String> list = Arrays.asList("hello", "world");
		Stream streamList = list.stream();
		
		AtomicInteger i = new AtomicInteger();
		Stream.generate(()-> {
			return i.getAndIncrement();
		})
		.limit(5)
		.forEach(System.out::println);
	}

	public void testIterate() {
		Stream<Integer> stream10 = Stream.iterate(0, n -> n + 1)
						.limit(5);
		stream10.forEach(System.out::println);
	}

	public void testMethod() {
		Integer[] arr = { 1, 2, 3, 4, 5 };
		long count = Stream.of(arr)
				.filter(i -> i % 2 == 0)
				.count();
		System.out.println("偶数数量：" + count);

		int num = Stream.of(arr)
				.filter(i -> i % 2 == 0)
				.findAny()
				.orElse(0);
		System.out.println("findAny:" + num);

		System.out.println("=== 去重示例 ===");
		Stream.of(1,1,2,3,3,4)
				.distinct()
				.forEach(System.out::println);

		System.out.println("=== sorted示例 ===");
		Stream.of(6,1,7,2,8,5)
		.sorted()
		.forEach(System.out::println);

		System.out.println("=== sorted示例2 ===");
		Stream.of(6,1,7,2,8,5)
		.sorted(Comparator.reverseOrder())
		.forEach(System.out::println);

		System.out.println("=== sorted示例3 ===");
		Stream.of(
				new Goods("iphoneX", 4000)
				, new Goods("mate30 pro", 5999)
				, new Goods("redmek20", 2999)
				)
		.sorted(Comparator.comparing(Goods::getPrice))
		.forEach(System.out::println);

		System.out.println("=== map示例 ===");
		List<Goods> list = Arrays.asList(
				new Goods("iphoneX", 4000)
				, new Goods("mate30 pro", 5999)
				, new Goods("redmek20", 2999)
				);
		list.stream()
			.map(Goods::getGoodsName)
			.forEach(System.out::println);

		List<String> goodsNameList = new ArrayList<>(list.size());
		for(Goods goods : list) {
			goodsNameList.add(goods.getGoodsName());
		}
	}



}
