package learn.java8.chapter7;

import junit.framework.TestCase;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Arrays;
import java.util.Comparator;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author tanghc
 */
public class CollectTest extends TestCase {
    @Data
    @AllArgsConstructor
    static class Goods {
        private String goodsName;
        private int price;
    }

    public void testAvg() {
        Stream<Goods> stream = Stream.of(
				new Goods("iphoneX", 4000)
				, new Goods("mate30 pro", 5999)
				, new Goods("redmek20", 2999)
				);

		List<Goods> list = stream.collect(Collectors.toList());
    }

    public void testSum() {
        double summingInt = Stream.of(1, 2, 3)
				.collect(Collectors.summingInt(val -> val));
		System.out.println("summingInt:" + summingInt);

		double summingLong = Stream.of(10L, 21L, 30L)
				.collect(Collectors.summingLong(val -> val));
		System.out.println("summingLong:" + summingLong);

		double summingDouble = Stream.of(0.1, 0.2, 0.3)
				.collect(Collectors.summingDouble(val -> val));
		System.out.println("summingDouble:" + summingDouble);

		List<Goods> list = Arrays.asList(
				new Goods("iphoneX", 4000)
				, new Goods("mate30 pro", 5999)
				, new Goods("redmek20", 2999)
				);

		double avgPrice = list.stream()
			.collect(Collectors.summingInt(goods -> goods.getPrice()));
		System.out.println("商品的总价格：" + avgPrice);
    }

    public void testSummarizing() {
        IntSummaryStatistics summarizingInt = Stream.of(1, 2, 3)
				.collect(Collectors.summarizingInt(val -> val));
		System.out.println("平均值:" + summarizingInt.getAverage());
		System.out.println("总个数:" + summarizingInt.getCount());
		System.out.println("总和:" + summarizingInt.getSum());
		System.out.println("最大值:" + summarizingInt.getMax());
		System.out.println("最小值:" + summarizingInt.getMin());
    }

    public void testJoin() {
        List<String> list = Arrays.asList("hello", "world");
        String str = list.stream().collect(Collectors.joining(","));
        System.out.println(str);

        String str2 = String.join(",", list);
        System.out.println(str2);
    }

    public void testMaxBy() {
        List<Goods> list = Arrays.asList(
				new Goods("iphoneX", 4000)
				, new Goods("mate30 pro", 5999)
				, new Goods("redmek20", 2999)
				);

		Goods maxPriceGoods = list.stream()
			.collect(
				Collectors.maxBy(
					Comparator.comparing(Goods::getPrice)
				)
			)
			.orElse(null);
		System.out.println("最贵的商品：" + maxPriceGoods);
    }

	public void testToCurrentMap() {
		List<Goods> list = Arrays.asList(
				new Goods("iphoneX", 4000)
				, new Goods("mate30 pro", 5999)
				, new Goods("redmek20", 2999)
		);
		ConcurrentMap<String, Integer> goodsMap = list.stream()
				.collect(
						Collectors.toConcurrentMap(Goods::getGoodsName, Goods::getPrice)
				);
		System.out.println(goodsMap);

	}

	public void testToCurrentMap2() {
		List<Goods> list = Arrays.asList(
				new Goods("iphoneX", 4000)
				, new Goods("mate30 pro", 5999)
				, new Goods("mate30 pro", 6000) // 这里有两个冲突了
				, new Goods("redmek20", 2999)
		);
		ConcurrentMap<String, Integer> goodsMap = list.stream()
				.collect(
						Collectors.toConcurrentMap(Goods::getGoodsName, Goods::getPrice, new BinaryOperator<Integer>() {
							@Override
							public Integer apply(Integer price1, Integer price2) {
								// 选择价格贵的返回
								return Math.max(price1, price2);
							}
						})
				);
		System.out.println(goodsMap);

	}



}
