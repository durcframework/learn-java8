package learn.java8.chapter4;

import junit.framework.TestCase;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * 比较器复合
 *
 * @author tanghc
 */
public class ComparatorTest extends TestCase {

    @Data @AllArgsConstructor
    static class Goods {
        private String name;
        private int price;
    }

    public void test1() {
        List<Goods> list = Arrays.asList(new Goods("mete30", 3999), new Goods("mete30 pro", 4999),
				new Goods("redmi k20", 2999), new Goods("iqoo", 2999), new Goods("iphone11", 5000));

		Comparator<Goods> comparatorForPrice = (Goods goods1, Goods goods2) -> {
			return Integer.compare(goods1.getPrice(), goods2.getPrice());
		};

		Comparator<Goods> comparatorForName = (Goods goods1, Goods goods2) -> {
			return goods1.getName().compareTo(goods2.getName());
		};

		// 把两个函数式接口进行复合，组成一个新的接口
		Comparator<Goods> finalComparator = comparatorForPrice.thenComparing(comparatorForName);
		list.sort(finalComparator);
		System.out.println(list);
    }
}
