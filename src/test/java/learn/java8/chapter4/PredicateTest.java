package learn.java8.chapter4;

import junit.framework.TestCase;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.function.Predicate;

/**
 * 谓词复合
 * @author tanghc
 */
public class PredicateTest extends TestCase {

    @Data @AllArgsConstructor
    static class Goods {
		private String name;
		private int price;
		// 库存
		private int storeCount;
	}

	public void test1() {
        Goods mete30pro = new Goods("mete30 pro", 4999, 111);
        Goods iphone11 = new Goods("iphone11", 5000, 444);

        Predicate<Goods> predicate = (goods) -> goods.price > 4000;

        System.out.println("mete30pro价格是否大于4000：" + predicate.test(mete30pro));

        Predicate<Goods> predicatePrice = (goods) -> goods.price > 6000;
        Predicate<Goods> predicateStore = (goods) -> goods.storeCount > 400;
        // 价格大于6000或库存大于400
        Predicate<Goods> predicateOr = predicatePrice.or(predicateStore);
        System.out.println("价格大于6000或库存大于400:" + predicateOr.test(iphone11));
    }
}
