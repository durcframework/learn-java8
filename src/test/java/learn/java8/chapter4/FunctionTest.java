package learn.java8.chapter4;

import junit.framework.TestCase;

import java.util.function.Function;

/**
 * Function复合
 * @author tanghc
 */
public class FunctionTest extends TestCase {

    public void test1() {
        Function<Integer, Integer> funMultiply = (input) -> input * 2;
        Function<Integer, Integer> funMinus = (input) -> input - 1;
        // input * 2 - 1
        Function<Integer, Integer> finalFun = funMultiply.andThen(funMinus);

        Integer result = finalFun.apply(2);
        System.out.println(result); // 3
    }

    public void test2() {
        Function<Integer, Integer> funMultiply = (input) -> input * 2;
        Function<Integer, Integer> funMinus = (input) -> input - 1;

        // （input - 1） * 2
        Function<Integer, Integer> finalFun = funMultiply.compose(funMinus);

        Integer result = finalFun.apply(2);
        System.out.println(result); // 2
    }

}
